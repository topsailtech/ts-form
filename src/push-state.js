/*

   <ts-form-push-state></ts-form-push-state>

   put this anywhere inside your form. a "submit" event will record the new url in the browser history.

   attributes:  replace-state (optional) - replace the current browser history entry rather than
                                           adding to the browser history


 // Be aware of   https://code.google.com/p/chromium/issues/detail?id=94369
 // For now, we are adding a "Vary: Accept" response header
 //     to the index response XHR to make pushState/replaceState work with the back button and XHR

*/

import WC from 'wc-util'

class TsFormPushState extends HTMLElement {

  connectedCallback(){
    this.form = WC.closest(this, "form");

    if (this.form && history.pushState){
      this.form.addEventListener("submit", _submitTriggered.bind(this));
    }
  }


}

customElements.define('ts-form-push-state', TsFormPushState)

function _submitTriggered(){
  var form_params = WC.serialize_as_string(this.form),
      action = this.form.getAttribute("action"), // don't use form.action, it's not consistent when no action attr given
      new_url = action.includes("?") ? action + '&' + form_params :
                                       action + '?' + form_params;

  if (this.hasAttribute("replace-state")){
    history.replaceState({}, "", new_url)
  } else {
    history.pushState({}, "", new_url)
  }
}