/*

   <ts-form-onchange-submit></ts-form-onchange-submit>

   put this anywhere inside your form, and "on change" this form will trigger a "submit" event.
   Also, the actual submit event will be trapped, so that no actual submit() will occur, unless
    you implement your own handler for the "submit" event.

*/

import WC from 'wc-util'

class TsFormOnchangeSubmit extends HTMLElement {

  connectedCallback(){
    this.form = WC.closest(this, "form");

    if (this.form){
      this.form.addEventListener("change", function(event){
        WC.fireEvent(this, "submit");
      });

      this.form.addEventListener("submit",function(event){
        event.preventDefault(); // prevents submit() function call (otherwise double submits possible)
      });
    }
  }

}

customElements.define('ts-form-onchange-submit', TsFormOnchangeSubmit);