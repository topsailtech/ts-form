/*

   <ts-form-disable-during-submit></ts-form-disable-during-submit>

   put this anywhere inside your form, and "on submit" this form will disable all buttons

*/

class TsFormDisableDuringSubmit extends HTMLElement {

  connectedCallback(){
    this.form = this.closest("form");

    if (this.form){
      this.form.addEventListener("submit", function(event){
        this.classList.add("ts-submitting")
      });
    }
  }

}

customElements.define('ts-form-disable-during-submit', TsFormDisableDuringSubmit);