In your rails project:

Edit /package.json:
- add to {"dependencies":{   }}
    "ts-form": "https://topsail_ro:tit1TROpw@bitbucket.org/topsailtech/ts-form.git"


Make sure you have a file /app/javascript/packs/ts-components.js
Add to that file
    import 'ts-form'


In your view (layout?), make sure to include
    <%= javascript_pack_tag 'ts-components' %>
    <%= stylesheet_pack_tag 'ts-components' %>


Run
    yarn

To re-fetch the latest version, run
    yarn upgrade ts-form